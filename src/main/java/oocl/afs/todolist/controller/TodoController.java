package oocl.afs.todolist.controller;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
//@CrossOrigin(origins = "http://localhost:3000")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    List<TodoResponse> getAll() {
        return todoService.findAll();
    }

    @PostMapping
    TodoResponse create(@RequestBody TodoCreateRequest todoRequest){
        Todo todo = TodoMapper.toEntity(todoRequest);
        Todo savedTodo = todoService.addTodo(todo);
        return TodoMapper.toResponse(savedTodo);
    }

    @PutMapping("/{id}")
    TodoResponse update(@PathVariable Long id, @RequestBody TodoCreateRequest todoRequest){
        Todo todo = TodoMapper.toEntity(todoRequest);
        todo.setId(id);
        Todo savedTodo = todoService.updateTodo(todo);
        System.out.println(savedTodo);
        return TodoMapper.toResponse(savedTodo);
    }

    @DeleteMapping
    boolean delete(){
        return false;
    }

}
