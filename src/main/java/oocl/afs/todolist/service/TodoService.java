package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoRepository;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> findAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoEtity -> TodoMapper.toResponse(todoEtity))
                .collect(Collectors.toList());
    }

    public Todo addTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    public Todo updateTodo(Todo todo) {
        return todoRepository.save(todo);
    }
}
